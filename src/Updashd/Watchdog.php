<?php
namespace Updashd;

use Predis\Client;
use Updashd\Scheduler\Scheduler;

class Watchdog {
    private $config;
    private $client;
    
    public function __construct ($config) {
        // Store the configuration
        $this->setConfig($config);
    
        // Create the Predis Client
        $client = new Client($this->getConfig('predis'));
        $this->setClient($client);
    }
    
    /**
     * Run the worker
     */
    public function run () {
        $scheduler = new Scheduler($this->getClient(), $this->getConfig('zone'));

        while (true) {
            $time = time();
            $zombieCount = $scheduler->processZombies($time);

            if ($zombieCount > 0) {
                printf("%s zombies: %s\n", date('Y-m-d H:i:s', $time), $zombieCount);
            }

            sleep(10);
        }
    }
    
    /**
     * Retrieve the config, or a key from it.
     * @param string $key
     * @return array
     */
    public function getConfig ($key = null) {
        if ($key && array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }
        
        return $this->config;
    }
    
    /**
     * @param array $config
     */
    public function setConfig ($config) {
        $this->config = $config;
    }
    
    /**
     * @return Client
     */
    public function getClient () {
        return $this->client;
    }
    
    /**
     * @param Client $client
     */
    public function setClient (Client $client) {
        $this->client = $client;
    }
}